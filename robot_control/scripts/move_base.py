#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry

class MoveBase():
    
    def __init__(self):
        rospy.init_node('move_robot')
        
        
    def go(self):
        pub = rospy.Publisher('cmd_vel', Twist, queue_size=10)
        vel = Twist()
        vel.linear.x = 1.0
        vel.angular.z = 1.0
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            pub.publish(vel)
            rate.sleep()

    def callback_odom(self, data):
        print(data.pose.pose.position)

    def get_pose(self):
        rospy.Subscriber('odom', Odometry, self.callback_odom)
        rospy.spin()

if __name__ == '__main__':
    r = MoveBase()
    r.get_pose()
    
        
