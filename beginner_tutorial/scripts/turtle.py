#!/usr/bin/env python
# license removed for brevity
import rospy
from geometry_msgs.msg import Twist

def talker():
    pub = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
    rospy.init_node('talker')
    vel = Twist()
    vel.linear.x = 1.0
    vel.angular.z = 1.0
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        pub.publish(vel)
        rate.sleep()


if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
